const path = require("path");
const fs = require('fs');
const CleanWebpackPlugin = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
module.exports = {
    mode: "development",
    devtool: "inline-source-map",
    entry: {
        ChatMain: "./src/index.ts",
        style: ["./src/less/style.less"]
    },
    output: {
        filename: "[name].bundle.js"
    },
    resolve: {
        extensions: [".ts", ".tsx", ".js"]
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: "ts-loader"
            },
            {
                test: /\.less$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    'less-loader',
                ]
            },
        ]
    },
    plugins: [
        new CleanWebpackPlugin("dist"),
        new MiniCssExtractPlugin({
            filename: "[name].bundle.css",
            chunkFilename: "[id].chunk.css"
        }),
        new HtmlWebpackPlugin({
            template: "./src/index.html",
            title: "mushroom data bind page",
            filename: "index.html"
        })
    ],
    devServer: {
        contentBase: path.join(__dirname, "dist"),
        compress: true,
    },
    watch: false,
};