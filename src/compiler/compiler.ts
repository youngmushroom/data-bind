import { IObserver } from "../observer";

export interface ICompiler {
    compile(htmlElement: DocumentFragment): void;
}
const SUPPORT: string[] = ["g-text", "g-value", "g-click", "g-change", "g-model"];
interface ICompilerFactroy {
    getCompiler(compilerType: string): IAttributeCompiler;
}
class DefaultCompilerFactory implements ICompilerFactroy {
    getCompiler(compilerType: string): IAttributeCompiler {
        throw new Error("Method not implemented.");
    }
}
class DefaultDomCompiler implements ICompiler {
    private factory: ICompilerFactroy;
    constructor() {
        this.factory = new DefaultCompilerFactory();
    }
    compile(fragment: DocumentFragment | HTMLElement): void {
        if (!fragment || !fragment.childNodes || fragment.childElementCount < 1) {
            return;
        }
        let child: Element;
        let children: HTMLCollection = fragment.children;
        for (let index: number = 0; index < children.length; index++) {
            child = children[index];
            this.compileCore(child as HTMLElement);
        }
    }
    private compileCore(element: HTMLElement): void {
        let attributes: NamedNodeMap = element.attributes;
        if (attributes.length < 1) {
            return;
        }
        let attribute: Attr;
        for (let index: number = 0; index < attributes.length; index++) {
            attribute = attributes[index];
            if (SUPPORT.indexOf(attribute.name.toLowerCase()) > -1) {
                let compiler: IAttributeCompiler = this.factory.getCompiler(attribute.name);
                compiler.compile(element);
            }
        }
    }
}

interface IAttributeCompiler {
    compile(element: HTMLElement): void;
}
abstract class AbstractCompiler implements IAttributeCompiler {
    private _element: HTMLElement;
    compile(element: HTMLElement): void {
        this.element = element;
    }
    public get element(): HTMLElement {
        return this._element;
    }
    public set element(element: HTMLElement) {
        this._element = element;
    }
}

class DefaultTextCompiler extends AbstractCompiler implements IObserver {
    constructor() {
        super();
    }
    update(obj: any, propertykey: string | number | symbol, oldValue: any): void {
        if (this.element) {
            this.element.innerText = undefined === obj ? obj[propertykey] : "";
        }
    }
    notify(): void {
        throw new Error("Method not implemented.");
    }
    compile(element: HTMLElement): void {
        super.compile(element);
    }
}
class DefaultClickCompiler extends AbstractCompiler implements IObserver {

    compile(element: HTMLElement): void {
        element.addEventListener("click", () => {
            //
        });
    }

    update(obj: any, propertykey: string | number | symbol, oldValue: any): void {

    }

    notify(): void {
        throw new Error("Method not implemented.");
    }
}