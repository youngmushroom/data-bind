type SymbolGUID = () => number;
var GUID: SymbolGUID = (() => {
    var _id: number = 0;
    return () => {
        return _id++;
    };
})();
export interface ISubject {
    proxy(obj: any): void;
    attach(observer: IObserver): void;
    detach(observer: IObserver): void;
    state(): SubjectState;
}
export interface IObserver {
    update(obj: any, propertykey: PropertyKey, oldValue: any): void;
    notify(): void;
}

enum SubjectState {
    Pendding,
    Changing,
    Changed,
    Notifing,
    Notified,
}
class DsefaultSubject implements ISubject {
    private _observers: IObserver[] = [];
    private _state: SubjectState = SubjectState.Pendding;
    private defaultSymbol: string = "_$$symbol$$__";
    public proxy(obj: any | Object): void {
        if (undefined === obj) {
            return;
        }
        if (obj.hasOwnProperty(this.defaultSymbol)) {
            return;
        }
        let type: string = typeof obj;
        if (["number", "string", "number"].indexOf(type) > -1) {
            return;
        }
        Object.defineProperty(obj, this.defaultSymbol, {
            value: GUID(),
            enumerable: false,
            writable: false,
            configurable: false
        });
        Object.keys(obj).forEach((propertyKey) => {
            let propertyValue: any = obj[propertyKey];
            if ("function" === typeof propertyValue) {
                return;
            }
            if ("object" === typeof propertyValue) {
                this.proxy(propertyValue);
            }
            if (!obj.hasOwnProperty(`_${propertyKey}`)) {
                obj[`_${propertyKey}`] = obj[propertyKey];
            }
            Object.defineProperty(obj, propertyKey, {
                set: (value: any) => {
                    let oldValue: any = obj[`_${propertyKey}`];
                    if (oldValue !== value) {
                        obj[`_${propertyKey}`] = value;
                        this.proxy(value);
                        this.notify(obj, propertyKey, oldValue);
                    }
                },
                get: () => {
                    return obj[`_${propertyKey}`];
                },
                enumerable: false,
                configurable: false,
            });
        });
    }

    private notify(obj: any, propertyKey: string, oldValue: any): void {
        if (this.observers && this.observers.length > 0) {
            this.observers.forEach((subject: IObserver) => {
                if (subject) {
                    subject.update(obj, propertyKey, oldValue);
                }
            });
        }
    }
    public attach(observer: IObserver): void {
        if (observer) {
            if (-1 === this.observers.indexOf(observer)) {
                this.observers.push(observer);
            }
        }
    }
    public detach(observer: IObserver): void {
        if (!this.observers || this.observers.length < 1) {
            return;
        }
        if (observer) {
            let indexOf: number = this.observers.indexOf(observer);
            if (indexOf > -1) {
                this.observers.splice(indexOf, 1);
            }
        }
    }
    private get observers(): IObserver[] {
        return this._observers;
    }
    public state(): SubjectState {
        return this._state;
    }
}

class DefaultObserver implements IObserver {
    private _id: number = -1;
    constructor(id: number) {
        //
        this._id = id;
    }
    update(obj: any, propertykey: string | number | symbol, value: any): void {
        // console.debug(`update: ${this._id}, ${propertykey as string}, ${obj[propertykey]}, ${value}`);
    }

    notify(): void {
        // console.debug(`notify: ${this._id}`);
    }

}
